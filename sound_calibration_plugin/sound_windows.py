# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging
import os
import time

import numpy as np
from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlButton
from pyforms.controls import ControlCombo
from pyforms.controls import ControlLabel
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlText
from pyforms.controls import ControlProgress

from pyforms import conf
from toolsR import SoundR

import sound_calibration_plugin.data_utils as dut
import sound_calibration_plugin.settings as settings
import sound_calibration_plugin.sound_utils as sut
from sound_calibration_plugin.default_values import DefaultValues
from sound_calibration_plugin.historyWindows.historyBroad_windows import HistoryBroad
from sound_calibration_plugin.historyWindows.historyPure_windows import HistoryPure
from sound_calibration_plugin.historyWindows.historyTrendAmp_windows import HistoryTrendAmp
from sound_calibration_plugin.historyWindows.historyTrendFreq_windows import HistoryTrendFreq
from sound_calibration_plugin.taskSound.task_pure import TaskPureTone
from sound_calibration_plugin.taskSound.task_broad import TaskBroadband
from sound_calibration_plugin.taskSound.task_amptrend import TaskAmpTrend
from sound_calibration_plugin.taskSound.task_freqtrend import TaskFreqTrend

logger = logging.getLogger(__name__)

class SoundWindow(BaseWidget):
    """
    See:
    - pyforms_generic_editor.models.projects.__init__.py
    - pyforms_generic_editor.models.projects.projects_window.py
    """

    def __init__(self, projects):
        BaseWidget.__init__(self, 'Sound Calibration', parent_win=projects.mainwindow)

        #self.set_margin(10)
        #self.setMaximumSize(800, 600)

        self.projects = projects

        # Load the last values
        self.defaultVariables = DefaultValues(self)
        self.values = self.defaultVariables.getValues()

        # -----  Definition of the forms fields -------
        # Calibration
        self._dbNeeds = ControlText('Decibel needs (dB):', default=self.values['_dbNeeds'], helptext='The decibels to obtain.')
        self._tolerance = ControlText('Tolerance (± dB):', default=self.values['_tolerance'], helptext='The decibel tolerance accepted.')
        self._loop = ControlText('Number of loop: ', default=self.values['_loop'], helptext='Number of records to use for each computation of decibels.')
        self._side = ControlCombo('Speaker\'s side: ', helptext = 'Which side you want to calibrate.')

        # Pure tone
        self._amp = ControlText('Starting amplitude', default=self.values['_amp'], helptext='The amplitude to use for the first loop.')
        self._freq = ControlText('Frequency signal(Hz):', default=self.values['_freq'], helptext='Frequency of the pure tone.')
        self._startPureTone = ControlButton('Run', helptext='Run the calibration with a pure tone.')
        self._fftPureTone = ControlButton('Spectrum pure tone', helptext='Play a pure tone and plot the spectogram recorded.')
        self._historyP = ControlButton('History', helptext='Show the data of past pure tone calibrations.')

        # Broadband noise
        self._ampBroad = ControlText('Starting amplitude', default=self.values['_ampBroad'], helptext='The amplitude to use for the first loop.')
        self._minBand = ControlText('Min frequency band(Hz):', default=self.values['_minBand'], helptext='Min frequency band.')
        self._maxBand = ControlText('Max frequency band(Hz):', default=self.values['_maxBand'], helptext='Max frequency band.')
        self._startBandNoise = ControlButton('Run', helptext = 'Run the calibration with a broadband noise.')
        self._fftBroadband = ControlButton('Spectrum broadband noise', helptext='Play a broadband noise and plot the spectogram recorded.')
        self._historyB = ControlButton('History', helptext='Show the data of past broadband calibrations.')

        # Trend Amplitude
        self._minAmp  = ControlText('Min amplitude:', default=self.values['_minAmp'],helptext='Starting amplitude.')
        self._maxAmp = ControlText('Max amplitude:', default=self.values['_maxAmp'], helptext='Ending amplitude.')
        self._nAmp = ControlText('Iteration:', default=self.values['_nAmp'], helptext='Iteration between max and min.')
        self._fixFreq = ControlText('Fixed frequency(Hz):', default=self.values['_fixFreq'], helptext='Fixed frequency.')
        self._startTrendAmp = ControlButton('Run', helptext='Run the calibration changing the amplitude.')
        self._historyTamp = ControlButton('History', helptext='Show the data of past Trend changing the amplitude.')

        # Trend Frequency
        self._minFreq = ControlText('Min frequency(Hz):', default=self.values['_minFreq'], helptext='Starting frequency.')
        self._maxFreq = ControlText('Max frequency(Hz):', default=self.values['_maxFreq'], helptext='Ending frequency.')
        self._nFreq = ControlText('Iteration:', default=self.values['_nFreq'], helptext='Iteration between max and min.')
        self._fixAmp = ControlText('Fixed amplitude:', default=self.values['_fixAmp'], helptext='Fixed amplitude.')
        self._startTrendFreq = ControlButton('Run', helptext='Run the calibration changing the frequency.')
        self._historyTfreq = ControlButton('History', helptext='Show the data of past Trend changing the frequency.')

        # Control sound card
        self._devOut = ControlCombo('Select the sound Card:',helptext='The sound card to calibrated.')
        self._fsOut = ControlText('Sample frequency (Hz):', default=self.values['_fsOut'], helptext='Sample rate of the sound card.')

        # Control Microphone
        self._sens = ControlText('Sensitivity (mV/Pa): ', default=self.values['_sens'], helptext='Sensitivity of the microphone.')
        self._gainM = ControlText('Gain Microphone (dB): ', default=self.values['_gainM'], helptext='Amplification of the microphone.')


        # Control DAQ
        self._fsIn = ControlText('Sample rate (Hz):', default=self.values['_fsIn'], helptext='Sample rate of the data acquisition card.')
        self._chDqa = ControlText('Channel: ', default=self.values['_chDqa'], helptext='Channel where the microphone is connected. ')
        self._secDaq = ControlText('Time acquisition (seconds): ', default=self.values['_secDaq'], helptext='Length of the signal to acquire.')

        # Results
        self._result = ControlText('The amplitude to use is: ',
                                   helptext='The amplitude to use into the protocols to obtain the same output db.')
        self._dbresult = ControlText('The decibel resulted is (dB): ', helptext='Decibels obtained with the amilitude resulted.')
        self._graph = ControlMatplotlib('Graph')
        self._progress = ControlProgress('Running...')

        # Labels
        self._titlePureTone = ControlLabel('Pure Tone Variables')
        self._titleBroadband = ControlLabel('Broadband Noise Variables')
        self._titleTrendAmp = ControlLabel('Trend changing the Amplitude with fixed Frequency')
        self._titleTrendFreq = ControlLabel('Trend changing the Frequency with fixed Amplitude')

        self.formset = [{
            'Sound Card Settings': [('_devOut', '_fsOut')],
            'DAQ Settings': [('_fsIn', '_chDqa', '_secDaq')],
            'Microphone Settings': [('_gainM', '_sens')],
            'Calibration': [('_dbNeeds', '_tolerance', '_loop', '_side')]
        }, {
            'Pure Tone': [(' ','_titlePureTone',' ','_fftPureTone') ,('_amp', '_freq',' '), (' ', '_startPureTone', ' ', '_historyP')],
            'Broadband Noise' : [(' ','_titleBroadband',' ', '_fftBroadband'), ('_ampBroad', '_minBand','_maxBand'), (' ', '_startBandNoise',' ','_historyB')],
            'Trend Amplitude': [(' ', '_titleTrendAmp', ' '), ('_minAmp', '_maxAmp','_nAmp'), ('_fixFreq', ' ', ' ', '_startTrendAmp', '_historyTamp')],
            'Trend Frequency': [(' ', '_titleTrendFreq', ' '), ('_minFreq', '_maxFreq', '_nFreq'), ('_fixAmp',' ',' ', '_startTrendFreq', '_historyTfreq')]
        },
        ('_result',' ', '_dbresult'),  '_progress', '_graph', ' ']

        # Hide the forms
        self._result.hide()
        self._dbresult.hide()
        self._progress.hide()

        # Prepare the forms
        self._listResults = []
        self.final_dbNeeds = float(self.values['_dbNeeds'])

        # Plot
        self.mode = 0
        self._graph.value = self._on_draw

        #Show the list of devices
        listdev =  SoundR.getDevicesNamesAndNumbers()
        for m in listdev:
            self._devOut.add_item(m, m)
        self._devOut.value = self.values['_devOut']

        # Define the sides
        self._side.add_item('L - R', 0)
        self._side.add_item('L', 1)
        self._side.add_item('R', 2)
        self._side.value = self.values['_side']

        # Define the button action
        self._startPureTone.value  = self.__buttonToneAction
        self._startBandNoise.value = self.__buttonBroadAction
        self._startTrendAmp.value  = self.__buttonTrendAmp
        self._startTrendFreq.value = self.__buttonTrendFreq
        self._fftPureTone.value    = self.__buttonFFTpureTone
        self._fftBroadband.value   = self.__buttonFFTbroadband
        self._historyB.value       = self.__buttonHistoryB
        self._historyP.value       = self.__buttonHistoryP
        self._historyTamp.value    = self.__buttonHistoryTamp
        self._historyTfreq.value   = self.__buttonHistoryTfreq

        # Load all the data
        self.myData = dut.SoundDataUtils()

        self._taskisRunning = False  # Flag to avoid run more than one task each time.

    # --------------------------------- RUN THE PURE TONE CALIBRATION -----------------------------------
    def __buttonToneAction(self):
        """Button action event for pure tone"""

        if settings.SOUND_WITHOUT_DAQ:
            self.warning("--> DAQ not installed: you are running just a simulation!!",  "Warning")
            # continue

        if self._taskisRunning: # STOP if one process is running
            self.warning("The calibration is running. Wait till the end.",  "Warning")
            return

        try:
            # ---- Convert strings to numbers -----
            # Pure tone variables
            init_amplitude = float(self._amp.value)
            freq = int(self._freq.value)

            self.final_dbNeeds = float(self._dbNeeds.value)
            tolerance = float(self._tolerance.value)
            loop = int(self._loop.value)

            # Microphone
            sens = float(self._sens.value)
            gainM = float(self._gainM.value)

            # Sound card
            FsOut = int(self._fsOut.value) # sample rate, depend on the sound card
            devOut = self._devOut.value

            # DAQ
            FsIn = int(self._fsIn.value)
            chDaq = int(self._chDqa.value)
            secDaq = int(self._secDaq.value)

            self.defaultVariables.save()

        except Exception as err:
            self.warning('Check the values!\n Error: ' + str(err), "Value Error")
            return

        try: # Run the task in background.
            self._listResults = []  # Reset the values
            self.__task = TaskPureTone(self)
            self.__task.runPureTone(init_amplitude, freq, settings.SECONDS_SOUND, tolerance, loop, sens, gainM, FsOut, devOut, FsIn, chDaq, secDaq)
        except Exception as err:
            self.warning(str(err), "Unexpected error")
            return


    # ------------------------------- RUN THE PURE TONE AND PLOT THE SPECTOGRAM -----------------------
    def __buttonFFTpureTone(self):
        """Button action: play and plot the spectogram of the pure tone"""

        if settings.SOUND_WITHOUT_DAQ:
            self.warning("--> DAQ not installed: you are running just a simulation!!", "Warning")
            # continue

        if self._taskisRunning: # STOP if one process is running
            self.warning("The calibration is running. Wait till the end.", "Warning")
            return

        # Hide the forms
        self._result.hide()
        self._dbresult.hide()

        try:
            # ---- Convert strings to numbers -----
            # Pure tone variables
            init_amplitude = float(self._amp.value)
            freq = int(self._freq.value)


            self.final_dbNeeds = float(self._dbNeeds.value)

            # Microphone
            sens = float(self._sens.value)

            # Sound card
            FsOut = int(self._fsOut.value) # sample rate, depend on the sound card
            devOut = int(self._devOut.value)

            # DAQ
            FsIn = int(self._fsIn.value)
            chDaq = int(self._chDqa.value)
            secDaq = int(self._secDaq.value)

            self.defaultVariables.save()

        except Exception as err:
            self.warning('Check the values!\n Error: ' + str(err), "Value Error")
            return

        try:
            # Initialization variables
            self._listResults = [] # reset the values

            # Create the sound server
            soundStream = SoundR(sampleRate=FsOut, deviceOut=devOut, channelsOut=2)
            # Convert the amplitude to db
            amp_db = 20 * np.log10(init_amplitude)
            # Generate your sound arrays
            s1, s2 = sut.genPureTone(settings.SECONDS_SOUND, FsOut, freq, 10 ** (amp_db / 20))
            vData = []
            # Play the sound
            soundStream.load(s1)
            soundStream.playSound()

            # Record the signal by mmcdaqR and save it
            if settings.SOUND_WITHOUT_DAQ: # <-- if SOUND_WITHOUT_DAQ = 1
                vData.append(s1)
                time.sleep(1)
            else:                           # <-- if SOUND_WITHOUT_DAQ = 0
                data, vTime = sut.scanSignal(chDaq, secDaq, FsIn, sens)
                vData.append(data)

            # Stop the sound
            soundStream.stopSound()
            time.sleep(1)

            # Compute the spectogram
            if settings.SOUND_WITHOUT_DAQ:
                self._listResults, self.vecFreq = sut.comp_fft(vData[0], FsOut) # <-- if SOUND_WITHOUT_DAQ = 1
            else:
                self._listResults, self.vecFreq = sut.comp_fft(vData[0], FsIn) # <-- if SOUND_WITHOUT_DAQ = 0

            self.mode = 4  # useful for the plot
            self._graph.value = self._on_draw  # Plot the results

        except Exception as err:
            self.warning(str(err), "Unexpected error")
            return

    # ------------------------------- RUN THE BROADBAND NOISE CALIBRATION -----------------------
    def __buttonBroadAction(self):
        """ Button Action event for broadband noise"""

        if settings.SOUND_WITHOUT_DAQ:
            self.warning("--> DAQ not installed: you are running just a simulation!!", "Warning")
            # continue

        if self._taskisRunning: # STOP if one process is running
            self.warning("The calibration is running. Wait till the end.", "Warning")
            return

        try:
            # ---- Convert strings to numbers -----
            # Pure tone variables
            ampBroad = float(self._ampBroad.value)
            minBand = int(self._minBand.value)
            maxBand = int(self._maxBand.value)
            band = [minBand, maxBand]

            self.final_dbNeeds = float(self._dbNeeds.value)
            tolerance = float(self._tolerance.value)
            loop = int(self._loop.value)

            # Microphone
            sens = float(self._sens.value)
            gainM = float(self._gainM.value)

            # Sound card
            FsOut = int(self._fsOut.value) # sample rate, depend on the sound card
            devOut = self._devOut.value

            # DAQ
            FsIn = int(self._fsIn.value)
            chDaq = int(self._chDqa.value)
            secDaq = int(self._secDaq.value)

            self.defaultVariables.save()

        except Exception as err:
            self.warning('Check the values!\n Error: ' + str(err), "Value Error")
            return

        try:# Run the task in background.
            self._listResults = []  # reset the values
            self.__task = TaskBroadband(self)
            self.__task.runBroadband(ampBroad, band, settings.SECONDS_SOUND, tolerance, loop, sens, gainM, FsOut, devOut, FsIn,
                                    chDaq, secDaq)
        except Exception as err:
            self.warning(str(err), "Unexpected error")
            return

    # ------------------------------- RUN THE BROADBAND NOISE AND PLOT THE SPECTOGRAM -----------------------
    def __buttonFFTbroadband(self):
        """Button action: play and plot the spectogram of the pure tone"""

        if settings.SOUND_WITHOUT_DAQ:
            self.warning("--> DAQ not installed: you are running just a simulation!!", "Warning")
            # continue

        if self._taskisRunning: # STOP if one process is running
            self.warning("The calibration is running. Wait till the end.", "Warning")
            return

        # Hide the forms
        self._result.hide()
        self._dbresult.hide()

        try:
            # ---- Convert strings to numbers -----
            # Pure tone variables
            ampBroad = float(self._ampBroad.value)
            minBand = int(self._minBand.value)
            maxBand = int(self._maxBand.value)
            band = [minBand, maxBand]

            self.final_dbNeeds = float(self._dbNeeds.value)

            # Microphone
            sens = float(self._sens.value)

            # Sound card
            FsOut = int(self._fsOut.value) # sample rate, depend on the sound card
            devOut = self._devOut.value

            # DAQ
            FsIn = int(self._fsIn.value)
            chDaq = int(self._chDqa.value)
            secDaq = int(self._secDaq.value)

            self.defaultVariables.save()

        except Exception as err:
            self.warning('Check the values!\n Error: ' + str(err), "Value Error")
            return

        try:
            # Initialization variables
            self._listResults = [] # reset the values

            # Create the sound server
            soundStream = SoundR(sampleRate=FsOut, deviceOut=devOut, channelsOut=2)
            # Convert the amplitude to db
            amp_db = 20 * np.log10(ampBroad)
            # Generate your sound arrays
            s1, s2 = sut.genBreadbandNoise(settings.SECONDS_SOUND, FsOut, band, 10 ** (amp_db / 20))
            vData = []
            # Play the sound
            soundStream.load(s1)
            soundStream.playSound()

            # Record the signal by mmcdaqR and save it
            if settings.SOUND_WITHOUT_DAQ: # <-- if SOUND_WITHOUT_DAQ = 1
                vData.append(s1)
                time.sleep(1)
            else:                           # <-- if SOUND_WITHOUT_DAQ = 0
                data, vTime = sut.scanSignal(chDaq, secDaq, FsIn, sens)
                vData.append(data)

            # Stop the sound
            soundStream.stopSound()
            time.sleep(1)

            # Compute the spectogram
            if settings.SOUND_WITHOUT_DAQ:
                self._listResults, self.vecFreq = sut.comp_fft(vData[0], FsOut) # <-- if SOUND_WITHOUT_DAQ = 1
            else:
                self._listResults, self.vecFreq = sut.comp_fft(vData[0], FsIn) # <-- if SOUND_WITHOUT_DAQ = 0

            self.mode = 4  # useful for the plot
            self._graph.value = self._on_draw  # Plot the results

        except Exception as err:
            self.warning(str(err), "Unexpected error")
            return

    # ------------------------------- RUN PURE TONES WITH DIFFFERENT AMPLITUDES -----------------------
    def __buttonTrendAmp(self):
        """Button action: play pure tones at different amplitudes and a fix frequency."""

        if settings.SOUND_WITHOUT_DAQ:
            self.warning("--> DAQ not installed: you are running just a simulation!!", "Warning")
            # continue

        if self._taskisRunning: # STOP if one process is running
            self.warning("The calibration is running. Wait till the end.", "Warning")
            return

        # Hide the forms
        self._result.hide()
        self._dbresult.hide()

        try:
            minAmp = float(self._minAmp.value)
            maxAmp = float(self._maxAmp.value)
            nAmp = int(self._nAmp.value)
            fixFreq = int(self._fixFreq.value)
            # Microphone
            sens = float(self._sens.value)
            gainM = float(self._gainM.value)
            # Sound card
            FsOut = int(self._fsOut.value)  # sample rate, depend on the sound card
            devOut = self._devOut.value
            # DAQ
            FsIn = int(self._fsIn.value)
            chDaq = int(self._chDqa.value)
            secDaq = int(self._secDaq.value)
            self.defaultVariables.save()

        except Exception as err:
            self.warning('Check the values!\n Error: ' + str(err), "Value Error")
            return

        try: # Run the task in background
            self._listResults = []  # reset the values
            self.vecAmp = []
            self.__task = TaskAmpTrend(self)
            self.__task.runAmpTrend(settings.SECONDS_SOUND, minAmp, maxAmp, nAmp, fixFreq, sens, gainM, FsOut, devOut, FsIn,
                                    chDaq, secDaq)
        except Exception as err:
            self.warning(str(err), "Unexpected error")
            return

    # ------------------------------- RUN PURE TONES WITH DIFFERENT FREQUENCIES -----------------------
    def __buttonTrendFreq(self):
        """Button action: play pure tones at different frequencies and a fix amplitude."""
        if settings.SOUND_WITHOUT_DAQ:
            self.warning("--> DAQ not installed: you are running just a simulation!!", "Warning")
            # continue

        if self._taskisRunning: # STOP if one process is running
            self.warning("The calibration is running. Wait till the end.", "Warning")
            return

        self._result.hide()
        self._dbresult.hide()

        try:
            # Conversion of the variables
            minFreq = float(self._minFreq.value)
            maxFreq = float(self._maxFreq.value)
            nFreq = int(self._nFreq.value)
            fixAmp = float(self._fixAmp.value)
            # Microphone
            sens = float(self._sens.value)
            gainM = float(self._gainM.value)
            # Sound card
            FsOut = int(self._fsOut.value)  # sample rate, depend on the sound card
            devOut = self._devOut.value
            # DAQ
            FsIn = int(self._fsIn.value)
            chDaq = int(self._chDqa.value)
            secDaq = int(self._secDaq.value)
            self.defaultVariables.save()

        except Exception as err:
            self.warning('Check the values!\n Error: ' + str(err), "Value Error")
            return

        try: # Run the task in background
            self._listResults = []  # reset the values
            self.vecFreq = []
            self.__task = TaskFreqTrend(self)
            self.__task.runFreqTrend(settings.SECONDS_SOUND, minFreq, maxFreq, nFreq, fixAmp, sens, gainM, FsOut, devOut, FsIn,
                                    chDaq, secDaq)
        except Exception as err:
            self.warning(str(err), "Unexpected error")
            return

    # ----------------- HISTORY --------------------

    # Open new Windows.
    def __buttonHistoryB(self): # Broadband
        if not os.path.exists(settings.SOUND_PLUGIN_BROADBAND_PATH):
            self.warning('No data saved.', "Error")
            return
        if not hasattr(self, 'broad_historyB'):
            self.broad_historyB = HistoryBroad(self)
            self.broad_historyB.show()
            self.broad_historyB.subwindow.resize(*conf.SOUND_HISTORY_BROAD_PLUGIN_WINDOW_SIZE)
        else:
            self.broad_historyB.show()

        return self.broad_historyB

    # Open new Windows.
    def __buttonHistoryP(self): # Puretone
        if not os.path.exists(settings.SOUND_PLUGIN_PURETONE_PATH):
            self.warning('No data saved.', "Error")
            return
        if not hasattr(self, 'broad_historyP'):
            self.broad_historyP = HistoryPure(self)
            self.broad_historyP.show()
            self.broad_historyP.subwindow.resize(*conf.SOUND_HISTORY_PURE_PLUGIN_WINDOW_SIZE)
        else:
            self.broad_historyP.show()

        return self.broad_historyP

    # Open new Windows.
    def __buttonHistoryTamp(self): # Trend amplitude
        if not os.path.exists(settings.SOUND_PLUGIN_TRENDAMP_PATH):
            self.warning( 'No data saved.', "Error")
            return
        else:
            l = [f for f in os.listdir(settings.SOUND_PLUGIN_TRENDAMP_PATH) if f.endswith('.npy')]
            if len(l) == 0:
                self.warning('No data saved.', "Error")
                return
        if not hasattr(self, 'broad_historyTamp'):
            self.broad_historyTamp = HistoryTrendAmp(self)
            self.broad_historyTamp.show()
            self.broad_historyTamp.subwindow.resize(*conf.SOUND_HISTORY_TREND_PLUGIN_WINDOW_SIZE)
        else:
            self.broad_historyTamp.show()

        return self.broad_historyTamp

    # Open new Windows.
    def __buttonHistoryTfreq(self):  # Trend Frequency
        if not os.path.exists(settings.SOUND_PLUGIN_TRENDFREQ_PATH):
            self.warning('No data saved.', "Error")
            return
        else:
            l = [f for f in os.listdir(settings.SOUND_PLUGIN_TRENDFREQ_PATH) if f.endswith('.npy')]
            if len(l) == 0:
                self.warning('No data saved.', )
                return
        if not hasattr(self, 'broad_historyTfreq'):
            self.broad_historyTfreq = HistoryTrendFreq(self)
            self.broad_historyTfreq.show()
            self.broad_historyTfreq.subwindow.resize(*conf.SOUND_HISTORY_TREND_PLUGIN_WINDOW_SIZE)
        else:
            self.broad_historyTfreq.show()

        return self.broad_historyTfreq

    # --------------------------------- FIGURES --------------------------------
    def _on_draw(self, figure):
        """ Redraws the figure
        """
        l = len(self._listResults)
        x = range(l)

        # Plot the threshold
        x2 = range(l + 5)
        thr = np.ones(l + 5) * self.final_dbNeeds

        figure.clear()
        axes = figure.add_axes([0.2, 0.2, 0.7, 0.5])
        axes.set_ylabel('SPL (dB)')
        if self.mode == 0: # No input
            axes.set_title('Waiting for the calibration...')
            axes.set_xlabel('Number of cycle')
            axes.plot(x2, thr, settings.SOUND_PLUGIN_COLOR_DEFAULT, label='dB target')
        elif self.mode == 1: # calibration
            axes.set_title('Calibration result')
            axes.set_xlabel('Number of cycle')
            axes.plot(x, self._listResults, settings.SOUND_PLUGIN_COLOR_TASK, label = 'Recorded dB')
            axes.plot(x2, thr, settings.SOUND_PLUGIN_COLOR_DEFAULT, label = 'dB target')
        elif self.mode == 2: # Trend Amplitude
            axes.set_title('Trend changing the Amplitude')
            axes.set_xlabel('Amplitude')
            axes.semilogx(self.vecAmp, self._listResults, settings.SOUND_PLUGIN_COLOR_TASK, label='Recorded dB')
            thr = np.ones(len(self.vecAmp)) * self.final_dbNeeds
            axes.semilogx(self.vecAmp, thr, settings.SOUND_PLUGIN_COLOR_DEFAULT, label='dB target')
        elif self.mode == 3:  # Trend Frequency
            axes.set_title('Trend changing the Frequency')
            axes.set_xlabel('Frequency (Hz)')
            axes.semilogx(self.vecFreq, self._listResults, settings.SOUND_PLUGIN_COLOR_TASK, label='Recorded dB')
            thr = np.ones(len(self.vecFreq)) * self.final_dbNeeds
            axes.semilogx(self.vecFreq, thr, settings.SOUND_PLUGIN_COLOR_DEFAULT, label='dB target')
        elif self.mode == 4:
            axes.set_title('Power spectrum of the recorded signal')
            axes.set_xlabel('Frequency (Hz)')
            axes.set_ylabel('Magnitude')
            axes.set_yscale('log')
            axes.set_xscale('log')
            axes.plot(self.vecFreq, self._listResults, settings.SOUND_PLUGIN_COLOR_TASK)
        axes.grid()
        axes.legend()

    # --------------------------- OTHER --------------------------
    def show(self):
        # Prevent the call to be recursive because of the mdi_area
        if hasattr(self, '_show_called'):
            BaseWidget.show(self)
            return
        self._show_called = True
        self.mainwindow.mdi_area += self
        del self._show_called

        self._locals = locals()
        self._globals = globals()

        # Control DAQ
        if settings.SOUND_WITHOUT_DAQ:
            self.warning('DAQ not used.', "INFO")

    def beforeClose(self):
        return False

    @property
    def mainwindow(self):
        return self.projects.mainwindow

