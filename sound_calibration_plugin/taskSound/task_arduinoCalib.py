# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import time

import numpy as np
import PyCmdMessenger
from threading import Thread
from scipy import signal as si

from sound_calibration_plugin.logbook import Logbook
from sound_calibration_plugin.exc_manager import method_log, log

import sound_calibration_plugin.settings as setting
import sound_calibration_plugin.sound_utils as sut
import datetime

logger = logging.getLogger(__name__)
CRED = '\033[46;1;41m'
CEND = '\033[0m'

BAUD_RATE = 9600 #  By default
PRESSURE_REF = 0.00002 #  20 uPa

MAX_LEVEL = 1
MIN_LEVEL = 45

class StateMachine():
    """
    This object contains the state machine description of the calibration task. 
    """
    
    # A dictionary containing the states and the transition conditions
    # from state to state:
    states = {'init': {'low': 'L', 'high': 'H'},
                'H': {'low': 'HL', 'high': 'H'},
                'L': {'low': 'L', 'high': 'LH'},
                'HL': {'low': 'HL', 'high': 'too_tight'},
                'LH': {'low': 'too_tight', 'high': 'LH'}}
    modify_guess = {'H': 5, 'L': -5, 'HL': 1, 'LH': -1}
    exit_states = ('calibrated', 'too_tight', 'min', 'max')
    initial_state = 'init'

    def __init__(self, tolerance, target):
        
        self.current_state = StateMachine.initial_state  # Initial state.
        self.tolerance = tolerance
        self.target = target
        self.guess = None
        self.running = False

    @method_log
    def next(self, result):
        """
        Advance to the next state and compute the next guess according to
        the rules codified in $states and $modify_guess.
        """

        if self.cal_finished(result):
            self.current_state = 'calibrated'
        else:
            check = 'high' if result > self.target else 'low'
            self.current_state = StateMachine.states[self.current_state][check]
        
        if self.current_state in self.modify_guess: 
            self.guess += StateMachine.modify_guess[self.current_state]
            # Checking the boundaries:
            if self.guess < MAX_LEVEL:
                self.guess = MAX_LEVEL
                if self.max_flag:
                    self.current_state = 'max'
                self.max_flag = True
            elif self.guess > MIN_LEVEL:
                self.guess = MIN_LEVEL
                if self.min_flag:   
                    self.current_state = 'min'
                self.min_flag = True
        if self.current_state in StateMachine.exit_states:
            self.running = False

    def start(self, guess):
        """
        Reset the state machine to the initial state so that
        it can be run again.
        """
        self.current_state = StateMachine.initial_state
        self.guess = guess
        self.running = True
        self.min_flag = False
        self.max_flag = False

    def cal_finished(self, result):
        """
        Returns True if calibration is achieved with the given $result.
        """
        return abs(self.target - result) <= self.tolerance

class ArduinoTaskThread(Thread):
    """
    This thread performs the actual calibration. It is designed as a state machine
    implementing the following algorithm: it starts calibration (self.STATE = 'init')
    by obtaining the recorded dBs coming from the microphone. It then checks
    whether the calibration is completed or not, and whether our target value
    is higher or lower than the obtained. According to that, it modifies the volume
    in the Arduino shield and starts again. If the record is higher, then lower,
    then higher again, it means that our specified tolerance is too tight.
    """
    def __init__(self, parameters):

        Thread.__init__(self)
        # Logbook object to save the obtained data (see logbook.py):
        self.log = Logbook(setting.SOUND_PLUGIN_ARDUINO_CAL, "calibration_logs.csv")
        # Add all parameters coming from the main window to the class:
        self.__dict__.update(parameters)
        # Init variables:
        self.guess_record = []
        self.last_result = None
        # State machine object (see StateMachine class above):
        self.sm = StateMachine(self.tolerance, self.dbTarget)
        # For simulation, put simulation=True:
        self.simulation = True
        self.fake_answers_generator = (elem for elem in [10,15,26,22,20])

    def run(self):
        """
        There are four exit states: 'finished' is when the calibration ended
        successfully; 'min' is when we reached minimum shield volume (30); 'max'
        is when we reached maximum shield value (1); 'too_tight' is when we couldn't
        complete the calibration due to the tolerance being too tight.
        """
        self.windows.mode = 1

        self.sm.start(self.starting_guess)
        while self.sm.running:
            self.guess_record.append(self.sm.guess)
            result = self._getdB()
            self.sm.next(result)
            logging.warning(f"\t State: {self.sm.current_state}")
            self.windows._listResults.append(result)
            self.windows._graph.value = self.windows._on_draw

        self.last_result = result
        logging.warning("-------------- CAL FINISHED ---------------------")
        logging.warning(f"\t CALIBRATION SUMMARY:")
        logging.warning(f"\t * Results [dB]: {self.windows._listResults}")
        lenResult = len(self.windows._listResults)
        logging.warning(f"\t * Number of iterations: {lenResult}")
        logging.warning(f"\t * Final state: {self.sm.current_state} at volume {self.sm.guess}")
        if self.sm.current_state == 'too_tight':
            self.windows.warning("Tolerance is too tight.", "Warning")
            logging.warning(f"\t * NOT CALIBRATED, not saved.")
        elif self.sm.current_state == 'max':
            self.windows.warning("MAX value reached.", "Warning")
            logging.warning(f"\t * NOT CALIBRATED, not saved.")
        elif self.sm.current_state == 'min':
            self.windows.warning("MIN value reached.", "Warning")
            logging.warning(f"\t * NOT CALIBRATED, not saved.")
        else: # that is, self.sm.current_state == 'finished'
            logging.warning(f"\t * CALIBRATED, values saved.")
            self._savelog()
            self._saveEEprom()
            if lenResult == 1:
                self.windows.info("The speaker is already calibrated.", "Success")
            else:
                self.windows.info(f"The speaker is calibrated after {len(self.windows._listResults) - 1} iterations.", "Success")
        logging.warning(f"------------------------------------------------")
        self.windows.mode = 1  # see sound_windows.py
        self.windows._graph.value = self.windows._on_draw  # Plot the results
        self.windows._taskisRunning = False
        logging.warning(f"Exiting thread ...")

    def _savelog(self):
        """
        Saves the calibration data in the logs, making use of the Logbook class. Only called
        when calibration finishes successfully.
        """
        if self.side == 0:
            side_str = "LR"
        elif self.side == 1:
            side_str = "L"
        else:
            side_str = "R"
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        data = [self.box, date, 
                side_str, self.dbTarget, 
                self.tolerance, round(self.last_result, 2), 
                self.guess_record[-1]]
        header = ["Box", "Date", "Side", "Target", "Tolerance", "FinalVal", "Volume"]
        self.log.save(data, header)

    def _saveEEprom(self):
        """
        Commands the arduino to save the calibration value in the EEPROM.
        Only called when the calibration has finished successfully.
        """
        logging.warning("\t Saving to EEPROM.")
        self.messenger.send("save_data", self.guess_record[-1])

    def _getdB(self):
        """
        Asks the Arduino for a sound and captures it with the mic + DAC; it does it
        a number of times specified by self.iterations and computes an average.
        If we are in simulation mode, returned answers are fake (of course!).
        Returns: the received dBs.
        """
        received_average = 0
        if not self.simulation:
            for _ in range(self.iterations):
                self.messenger.send("send_volume", self.sm.guess, self.side)
                time.sleep(0.1)
                self.messenger.send("play_sound1")
                time.sleep(0.15)
                data, _ = sut.scanSignal(self.chDaq, self.secDaq, self.FsIn, self.sens)
                received_average += self._computedB(data) / self.iterations
        else:
            received_average = next(self.fake_answers_generator)
        logging.warning(f"\t {CRED}Received dB's: {received_average}, volume: {self.sm.guess}{CEND}")

        return received_average

    def _computedB(self, data):
        """
        Computes the sound level in dB of a white noise signal (whole available band).
        - data: the signal to transform.
        returns:
        - the value in dBs referred to 20 uPa.
        """
        # Compute the average of the incoming noise signal and receive it in [Pa].
        # First we remove infs, which are sometimes present and throw everything off:
        aux = [elem for elem in data if not np.isinf(elem)]
        # Pass the signal through a Butterworth filter with a 1 kHz cuttoff to remove 50 Hz noise:
        filtered_signal = ArduinoTaskThread.butter_highpass_filter(aux, 1000, 500000)
        average = np.average(np.sqrt(np.square(filtered_signal)))
        logging.warning(f"\t Received average: {average}")
        # Divide by the amplifier gain:
        pa = average / 100
        logging.warning(f"\t Sound pressure [Pa]: {pa}")

        # Convert to dBs referred to 20 uPa and return:
        return 20 * np.log10(pa / PRESSURE_REF)

    @staticmethod
    def butter_highpass(cutoff, fs, order=5):
        """
        This method computes the coefficients of a Butterworth high-pass
        filter.
         - cutoff: cutoff frequency in Hz
         - fs: sampling frequency of the signal (remember that this is a digital filter)
         - order: filter order
        returns:
         - numerator (b) and denominator (a) of the filter expression.
        """
        nyq = 0.5 * fs
        normal_cutoff = cutoff / nyq
        b, a = si.butter(order, normal_cutoff, btype='high', analog=False)
        return b, a

    @staticmethod
    def butter_highpass_filter(data, cutoff, fs, order=5):
        """
        This method applies a high-pass filter to incoming data.
        Returns: the filtered signal.
        """
        b, a = ArduinoTaskThread.butter_highpass(cutoff, fs, order=order)
        filtered = si.filtfilt(b, a, data)
        return filtered

    @property
    def starting_guess(self):
        last_volume = self.log.obtain_latest_result(self.box, self.side)
        if last_volume is None:
            # If for whatever reason the last value can't be recovered, start at 25:
            last_volume = 25
        return last_volume

class TaskArduinoCalib:
    """
    Arduino calibration procedure. It activates a thread so as not to block
    the main GUI window.
    """

    def __init__(self, windows, parameters: dict):
        """
        This object represents a calibration task. The $parameters dict contains all relevant
        data coming from the main window user inputs (see sound_windows.py); when initialized, 
        the task tried to establish contact with the Arduino device (which has to be in calibration mode), and
        if the contact is acknowledged, we will receive a box name and we can start the task itself, which 
        is basically a state machine inside a thread (see ArduinoTaskThread above).
        """

        self.windows = windows  # Variable that holds the main window, used to plot the results.
        self.windows._taskisRunning = True

        try:
            # Init comms with Arduino:
            arduino = PyCmdMessenger.ArduinoBoard(parameters['device'], baud_rate=BAUD_RATE)
            # These commands have to match a similar structure inside the Arduino sketch:
            commands = [["play_sound1",""],
                        ["play_sound2",""],
                        ["device_ready",""],
                        ["error","s"],
                        ["send_volume", "ii"],
                        ["cmd_box", "s"],
                        ["save_data", "i"]]
            # Initialize the messenger:
            self.messenger = PyCmdMessenger.CmdMessenger(arduino, commands)
            # Acknowledge:
            self.messenger.send("device_ready")
            # Receive the box number back:
            _, box, _ = self.messenger.receive()
            box_str = box[0]
        except Exception as err:
            self.windows._taskisRunning = False
            self.windows.critical("I couldn't find the Arduino. Try changing the port. \n "
                                 "E: task_arduino_cal", "Error")
        else:
            logging.warning(f"------------- STARTING CALIBRATION ----------------")
            logging.warning(f"\t Box: {box}")
            # Init the thread that contains the task:
            parameters.update(box=box_str, messenger=self.messenger, windows=self.windows)
            thread = ArduinoTaskThread(parameters)
            thread.setDaemon(True)
            thread.start() # And start it (this will run the $run method inside).
