# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pyforms import conf
from toolsR import SoundR
import sound_calibration_plugin.sound_utils as sut
import numpy as np, time
import sound_calibration_plugin.settings as setting
import logging
logger = logging.getLogger(__name__)

from AnyQt.QtCore import QTimer, QEventLoop

class TaskBroadband:
    """ Run the task of broadband noise. """

    def __init__(self, windows):
        self.windows = windows # variable to control the variables and methods od sound_windows.py
        # Loop for the task
        self._timerBroad = QTimer()

    def runBroadband(self, ampBroad, band, seconds, tolerance, loop, sens, gainM, FsOut, devOut, FsIn,
                                    chDaq, secDaq):
        """ Start the task. """
        self.windows._taskisRunning = True
        self.ampBroad = ampBroad
        self.band = band
        self.seconds = seconds
        self.tolerance = tolerance
        self.loop = loop
        self.sens = sens
        self.gainM = gainM
        self.FsOut = FsOut
        self.devOut = devOut
        self.FsIn = FsIn
        self.chDaq = chDaq
        self.secDaq = secDaq
        # Start
        # Initialization variables
        self.result_db = 0
        self.last_result_db = 0
        self.counterLoop = 0 # counter to use instead using for
        self._stop = False # Flag
        self.runTask(True) # Run the task
        self._timerBroad.timeout.connect(self.runTask)
        if not self._stop:
            self._timerBroad.start(setting.SOUND_PLUGIN_REFRESH_RATE) # Run the loop

    def runTask(self, startP = False):
        """ Qt Loop: it is running as loop till the variable _stop = True. """
        try:
            if startP:
                # Create the sound server
                self.soundStream = SoundR(sampleRate=self.FsOut, deviceOut=self.devOut, channelsOut=2)
                # Convert the amplitude to db
                self.amp_db = 20 * np.log10(self.ampBroad)
                # Progress bar
                self.windows._progress.value = 1
                self.windows._progress.max = 10
                self.windows._progress.show()

            if self.counterLoop == 0:
                # Generate your sound arrays
                print('')
                print('new cycle')
                print('amplitude:', 10 ** (self.amp_db / 20))
                print('amplitude in arbitrary log scale:', self.amp_db)
                self.s1, self.s_zeros = sut.genBreadbandNoise(self.seconds, self.FsOut, self.band, 10 ** (self.amp_db / 20))
                self.vData = []

            if self.counterLoop < self.loop:  # play the sound and record it loop times

                # Play the sound
                if self.windows._side.value == 0:  # both sides
                    self.soundStream.load(self.s1)
                elif self.windows._side.value == 1:  # left speaker
                    self.soundStream.load(self.s1, self.s_zeros)
                elif self.windows._side.value == 2:  # right speaker
                    self.soundStream.load(self.s_zeros, self.s1)
                self.soundStream.playSound()

                # Record the signal by mmcdaqR and save it
                if setting.SOUND_WITHOUT_DAQ: # <-- if SOUND_WITHOUT_DAQ = 1
                    self.vData.append(self.s1)
                    time.sleep(1)
                else:                           # <-- if SOUND_WITHOUT_DAQ = 0
                    data, vTime = sut.scanSignal(self.chDaq, self.secDaq, self.FsIn,
                                                 self.sens)
                    self.vData.append(data)

                # Stop the sound
                self.soundStream.stopSound()
                time.sleep(0.1)
                self.counterLoop += 1

            else:
                self.windows._progress += 1 # progress bar
                self.counterLoop = 0
                if setting.SOUND_WITHOUT_DAQ: # <-- if SOUND_WITHOUT_DAQ = 1
                    self.result_db = sut.droadband_db(self.vData, self.band, self.FsOut, gain_michrophone=self.gainM)
                else:                           # <-- if SOUND_WITHOUT_DAQ = 0
                    self.result_db = sut.droadband_db(self.vData, self.band, self.FsIn, gain_michrophone=self.gainM)
                self.windows._listResults.append(self.result_db)  # list to plot

                print('measured db:', self.result_db)

                # Check the dB values and fix the new amplitude
                diff_db = self.windows.final_dbNeeds - self.result_db
                if abs(diff_db) > self.tolerance:
                    self.amp_db = self.amp_db + diff_db
                    print('difference db:', diff_db)
                else:
                    self._stop = True

                # Check amplitude <= 1
                if (10 ** (self.amp_db / 20)) > 1.0:
                    self.amp_db = None
                    self._stop = True

                # Check the saturation
                if abs(self.result_db - self.last_result_db) < self.tolerance:
                    self.amp_db = None
                    self._stop = True
                else:
                    self.last_result_db = self.result_db


                if self._stop:
                    # Show the final results

                    # Show the forms
                    self.windows._result.show()
                    self.windows._dbresult.show()
                    self.windows._progress.value = 10

                    if self.amp_db is None:
                        # Display result
                        self.windows.warning("The speaker saturated!", "Warning")
                        self.windows._result.value = 'NaN'
                        self.windows._dbresult.value = 'NaN'
                    else:
                        lenResult = len(self.windows._listResults)
                        if lenResult == 1:
                            self.windows.info("The speaker is already calibrated.", "Success")
                        else:
                            self.windows.info("The speaker is calibrated correctly after "
                                                    + str(len(self.windows._listResults) - 1) + " iterations.", "Success")
                        self.windows._result.value = str(round(10 ** (self.amp_db / 20), 5))
                        self.windows._dbresult.value = str(round(self.result_db, 2))

                        # Save the results
                        self.windows.myData.saveBroadbandResult(self.band[0], self.band[1], self.loop, 10 ** (self.amp_db / 20),
                                                                self.result_db, self.windows._side.text, self.windows._devOut.text)

                    self.windows.mode = 1  # useful for the plot
                    self.windows._graph.value = self.windows._on_draw  # Plot the results
                    self._timerBroad.stop()
                    self.windows._taskisRunning = False
                    self.windows._progress.hide()

            QEventLoop()

        except Exception as err: # Catch the errors
            self._timerBroad.stop()
            self._stop = True
            self.windows._taskisRunning = False
            self.windows._progress.hide()
            logger.error(str(err), exc_info=True)
            self.windows.critical("Unexpected error while running sound calibration. Pleas see log for more details.\n"
                                 "E: task_broad", "Error")


