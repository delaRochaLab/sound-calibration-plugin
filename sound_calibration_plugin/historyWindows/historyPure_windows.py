#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
from pyforms.basewidget import BaseWidget
from pyforms.controls 	import ControlList
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlButton
from pyforms.controls import ControlCombo

import sound_calibration_plugin.data_utils as dut
import sound_calibration_plugin.settings as settings
import numpy as np

logger = logging.getLogger(__name__)


class HistoryPure(BaseWidget):
    """ Show the hisoty of the pure tone calibration. """

    def __init__(self, projects):
        BaseWidget.__init__(self, 'History Pure tone noise', parent_win = projects.mainwindow)
        self.projects = projects

        # Load data
        self.__loadDataP()

        # Definition of the forms fields
        self._graphP = ControlMatplotlib('Graph')
        self._listP = ControlList('History data')
        self._deleteP = ControlButton('Delete...')
        self._deleteP.value = self.__buttonDelete


        # Filter plot
        self._filter = ControlCombo('Sound card:', helptext='Select the way to plot the results.')
        self._filter.add_item('All', 0)
        self._filter.value = 0
        self._filter.changed_event = self._comboChangeEvent
        self.indexes = []


        self.formset = [(' ', '_filter', '_deleteP'),('_listP'), '=' , ('_graphP')]

        # List proprety
        self._listP.readonly = True
        self._listP.horizontal_headers = ['#', 'Date', 'Frequency', 'Loop', 'Amplitude', 'dB resulted',
                                          'Side', 'Sound Card']
        self._listP.set_sorting_enabled(True)

        # Graph proprety
        self.showHistoryList()
        self.__setFiletr()

    def __setFiletr(self):
        """ Create the list of sound card from the data"""
        for sc in set(self.vecDataP[6]):
            self._filter.add_item(str(sc), str(sc))

    def __buttonDelete(self):
        """ Click to the button"""
        idx = self.input_int("Write the index # that you want to delete.", "Index", min = 0)
        if idx is not None:
            checkErr = self.myDataP.removePuretoneResult(self.indexes[idx])
            if checkErr is None:
                self.showHistoryList()
                self.__setFiletr()
                self.info('Data deleted.', 'Info')
            else:
                self.warning('Incorrect index. Please try again. \n Error:{0}'.format(str(checkErr)), "Unexpected error")

    def _comboChangeEvent(self):
        """ When you choose a soundcard"""
        # Graph proprety
        self._graphP.value = self.__on_draw
        self.showHistoryList()
        self.__setFiletr()

    def showHistoryList(self):
        """ Upload the list"""
        self.__loadDataP()
        # Show historyWindows
        self._listP.clear()

        self.indexes = range(self.vecDataP[0].size)
        if self._filter.value != 0: # Sound card is selected
            self._counter = 0
            idx = np.where(self.vecDataP[6] == self._filter.value) # find the indexes of that soundcard
            self.indexes = idx[0]

        self._counter = 0
        for i in self.indexes:
            row = [self._counter, str(self.vecDataP[0][i])]
            row += [str(self.vecDataP[k][i]) for k in range(1,7)]
            row += []  # date
            self._listP += row
            self._counter += 1

        # Graph proprety
        self._graphP.value = self.__on_draw

    def __loadDataP(self):
        # Load data
        self.myDataP = dut.SoundDataUtils()
        self.vecDataP = self.myDataP.getPuretoneResult()

    def __on_draw(self, figure):
        """ Redraws the figure
        """
        self.__loadDataP()

        vec = self.vecDataP[3]
        if self._filter.value != 0: # Sound card is selected
            idx = np.where(self.vecDataP[6] == self._filter.value) # find the indexes of that soundcard
            vec = np.array([])
            for i in idx[0]:
                vec = np.append(vec, self.vecDataP[3][i]) # select only the data with the same soundcard

        length = len(vec)
        x = range(length)

        figure.clear()
        axes = figure.add_axes([0.2, 0.2, 0.7, 0.5])
        axes.set_ylabel('SPL (dB)')
        axes.set_title('History')
        axes.set_xlabel('#')
        axes.plot(x, vec, settings.SOUND_PLUGIN_COLOR_HISTORY, label='General historyWindows')

        axes.grid()
        axes.legend()


    def show(self):
        # Prevent the call to be recursive because of the mdi_area
        if hasattr(self, '_show_called'):
            BaseWidget.show(self)
            return
        self._show_called = True
        self.mainwindow.mdi_area += self
        del self._show_called

        self._locals = locals()
        self._globals = globals()

        self.showHistoryList()
        self.__setFiletr()

    def beforeClose(self):
        return False

    @property
    def mainwindow(self):
        return self.projects.mainwindow
