#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from pyforms.basewidget import BaseWidget
from pyforms.controls 	import ControlFile
from pyforms.controls   import ControlMatplotlib
from pyforms.controls   import ControlLabel

import sound_calibration_plugin.data_utils as dut
import sound_calibration_plugin.settings as settings

logger = logging.getLogger(__name__)


class HistoryTrendFreq(BaseWidget):
    """ Show the History of trend frequency. """
    def __init__(self, projects):
        BaseWidget.__init__(self, 'History Trend frequency', parent_win = projects.mainwindow)
        self.projects = projects

        # Definition of the forms fields
        self._graphTf = ControlMatplotlib('Graph')
        self._fileTf = ControlFile('Choose file:')
        self._soundcard = ControlLabel(' ')

        self.formset = [('_fileTf'),('_soundcard'), ('_graphTf')]

        self._fileTf.value = self.__loadLastFreq() # Load last data
        self._soundcard.value = 'Sound card used: ' + str(self.vecDataFreq[3])

        # Graph proprety
        self._graphTf.value = self.__on_draw
        self._fileTf.changed_event = self.__click # catch the change event

    def __click(self):
        """ Load the data and plot """
        file = self._fileTf.value
        self.myDataFreq = dut.SoundDataUtils()
        try:
            self.vecDataFreq = self.myDataFreq.getFreq(file)
            self._graphTf.value = self.__on_draw
            self._soundcard.value = 'Sound card used: ' + str(self.vecDataFreq[3])
        except Exception as err:
            self.warning('The file has to be npy-file or the format is not correct. \n Error: ' + str(err), "File Error")
            return

    def __loadLastFreq(self):
        """ Load data """
        self.myDataFreq = dut.SoundDataUtils()
        self.vecDataFreq, pathLastFile = self.myDataFreq.getLastFreq()
        return pathLastFile


    def __on_draw(self, figure):
        """ Redraws the figure
        """
        vec = self.vecDataFreq[2]
        x   = self.vecDataFreq[1]

        figure.clear()
        axes = figure.add_axes([0.2, 0.2, 0.7, 0.5])
        axes.set_ylabel('SPL (dB)')
        axes.set_title('Trend at amplitude ' + str(self.vecDataFreq[0]) + ' ('+ str(len(vec)) +' repetitions)')
        axes.set_xlabel('Frequency')
        axes.semilogx(x, vec, settings.SOUND_PLUGIN_COLOR_HISTORY, label='Trend')

        axes.grid()
        axes.legend()


    def show(self):
        # Prevent the call to be recursive because of the mdi_area
        if hasattr(self, '_show_calledTf'):
            BaseWidget.show(self)
            return
        self._show_calledTf = True
        self.mainwindow.mdi_area += self
        del self._show_calledTf

        self._locals = locals()
        self._globals = globals()

        self._fileTf.value = self.__loadLastFreq()  # Load last data
        self._graphTf.value = self.__on_draw

    def beforeClose(self):
        return False

    @property
    def mainwindow(self):
        return self.projects.mainwindow
