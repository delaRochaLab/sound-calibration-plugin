#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
from pyforms.basewidget import BaseWidget
from pyforms.controls 	import ControlList
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlButton
from pyforms.controls import ControlCombo

import sound_calibration_plugin.data_utils as dut
import sound_calibration_plugin.settings as settings
import numpy as np

logger = logging.getLogger(__name__)


class HistoryBroad(BaseWidget):
    """ Show the hisoty of broadband calibration. """

    def __init__(self, projects):
        BaseWidget.__init__(self, 'History Boradband noise', parent_win = projects.mainwindow)
        self.projects = projects

        # Load data
        self.__loadDataB()

        # Definition of the forms fields
        self._graphB = ControlMatplotlib('Graph')
        self._listB = ControlList('History data')
        self._deleteB = ControlButton('Delete...')
        self._deleteB.value = self.__buttonDelete

        # Filter plot
        self._filter = ControlCombo('Sound card:', helptext='Select the way to plot the results.')
        self._filter.add_item('All', 0)
        self._filter.value = 0
        self._filter.changed_event = self._comboChangeEvent
        self.indexes = []

        self.formset = [(' ', '_filter', '_deleteB'), ('_listB'), '=' , ('_graphB')]

        # List proprety
        self._listB.readonly = True
        self._listB.horizontal_headers = ['#', 'Date', 'MinFreq', 'MaxFreq', 'Loop', 'Amplitude', 'dB resulted',
                                          'Side','Sound Card']
        self._listB.set_sorting_enabled(True)

        # Graph proprety
        self.showHistoryList()
        self.__setFiletr()

    def __setFiletr(self):
        """ Create the list of sound card from the data"""
        for sc in set(self.vecDataB[7]):
            self._filter.add_item(str(sc), str(sc))

    def __buttonDelete(self):
        """ Click to the button"""
        idx = self.input_int("Write the index # that you want to delete.", "Index", min=0)
        if idx is not None:
            checkErr = self.myDataB.removeBroadbandResult(self.indexes[idx])
            if checkErr is None:
                self.showHistoryList()
                self.__setFiletr()
                self.info('Data deleted.', 'Info')
            else:
                self.warning('Incorrect index. Please try again. \n Error:{0}'.format(str(checkErr)),
                             "Unexpected error",)

    def _comboChangeEvent(self):
        """ When you choose a soundcard"""
        # Graph proprety
        self._graphB.value = self.__on_draw
        self.showHistoryList()
        self.__setFiletr()

    def showHistoryList(self):
        """ Upload the list"""
        self.__loadDataB()
        # Show historyWindows
        self._listB.clear()

        self.indexes = range(self.vecDataB[0].size)
        if self._filter.value != 0: # Sound card is selected
            self._counter = 0
            idx = np.where(self.vecDataB[7] == self._filter.value) # find the indexes of that soundcard
            self.indexes = idx[0]

        self._counter = 0
        for i in self.indexes:
            row = [self._counter, str(self.vecDataB[0][i])]
            row += [str(self.vecDataB[k][i]) for k in range(1, 8)]
            row += []  # date
            self._listB += row
            self._counter += 1

        # Graph proprety
        self._graphB.value = self.__on_draw

    def __loadDataB(self):
        """ Load data """
        self.myDataB = dut.SoundDataUtils()
        self.vecDataB = self.myDataB.getBroadbandResult()

    def __on_draw(self, figure):
        """ Redraws the figure
        """
        self.__loadDataB()

        vec = self.vecDataB[4]
        if self._filter.value != 0: # Sound card is selected
            idx = np.where(self.vecDataB[7] == self._filter.value) # find the indexes of that soundcard
            vec = np.array([])
            for i in idx[0]:
                vec = np.append(vec, self.vecDataB[4][i]) # select only the data with the same soundcard

        l = len(vec)
        x = range(l)

        figure.clear()
        axes = figure.add_axes([0.2, 0.2, 0.7, 0.5])
        axes.set_ylabel('SPL (dB)')
        axes.set_title('History')
        axes.set_xlabel('#')
        axes.plot(x, vec, settings.SOUND_PLUGIN_COLOR_HISTORY, label='General historyWindows')

        axes.grid()
        axes.legend()


    def show(self):
        #Prevent the call to be recursive because of the mdi_area
        if hasattr(self, '_show_calledB'):
            BaseWidget.show(self)
            return
        self._show_calledB = True
        self.mainwindow.mdi_area += self
        del self._show_calledB

        self._locals = locals()
        self._globals = globals()

        self.showHistoryList()
        self.__setFiletr()

    def beforeClose(self):
        return False

    @property
    def mainwindow(self):
        return self.projects.mainwindow
