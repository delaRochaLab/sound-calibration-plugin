#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlFile
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlLabel

import sound_calibration_plugin.data_utils as dut
import sound_calibration_plugin.settings as settings

logger = logging.getLogger(__name__)


class HistoryTrendAmp(BaseWidget):
    """ Show the hisoty of a trend amplitude. """

    def __init__(self, projects):
        BaseWidget.__init__(self, 'History Trend amplitude', parent_win=projects.mainwindow)
        self.projects = projects

        # Definition of the forms fields
        self._graphTa = ControlMatplotlib('Graph')
        self._fileTa = ControlFile('Choose file:')
        self._soundcard = ControlLabel(' ')


        self.formset = [('_fileTa'), ('_soundcard'), ('_graphTa')]

        self._fileTa.value = self.__loadLastAmp()  # Load last data
        self._soundcard.value = 'Sound card used: ' + str(self.vecDataAmp[3])

        # Graph proprety
        self._graphTa.value = self.__on_draw
        self._fileTa.changed_event = self.__click  # catch the change event

    def __click(self):
        """ Load the data and plot """
        file = self._fileTa.value
        self.myDataAmp = dut.SoundDataUtils()
        try:
            self.vecDataAmp = self.myDataAmp.getAmp(file)
            self._graphTa.value = self.__on_draw
            self._soundcard.value = 'Sound card used: ' + str(self.vecDataAmp[3])
        except Exception as err:
            self.warning('The file has to be npy-file or the format is not correct. \n Error: ' + str(err), "File Error")
            return

    def __loadLastAmp(self):
        """ Load data """
        self.myDataAmp = dut.SoundDataUtils()
        self.vecDataAmp, pathLastFile = self.myDataAmp.getLastAmp()
        return pathLastFile

    def __on_draw(self, figure):
        """ Redraws the figure
        """
        vec = self.vecDataAmp[2]
        x = self.vecDataAmp[1]

        figure.clear()
        axes = figure.add_axes([0.2, 0.2, 0.7, 0.5])
        axes.set_ylabel('SPL (dB)')
        axes.set_title('Trend at Frequency ' + str(self.vecDataAmp[0]) + 'Hz (' + str(len(vec)) + ' repetitions)')
        axes.set_xlabel('Amplitude')
        axes.semilogx(x, vec, settings.SOUND_PLUGIN_COLOR_HISTORY, label='Trend')

        axes.grid()
        axes.legend()

    def show(self):
        # Prevent the call to be recursive because of the mdi_area
        if hasattr(self, '_show_calledTa'):
            BaseWidget.show(self)
            return
        self._show_calledTa = True
        self.mainwindow.mdi_area += self
        del self._show_calledTa

        self._locals = locals()
        self._globals = globals()

        self._fileTa.value = self.__loadLastAmp()  # Load last data
        self._graphTa.value = self.__on_draw

    def beforeClose(self):
        return False

    @property
    def mainwindow(self):
        return self.projects.mainwindow
