import csv
import logging
import os
from pathlib import Path

class Logbook:
    """
    This class manages the calibration log for the Arduino devices.
    """

    def __init__(self, path:str , filename: str):
        self._path = path
        self._filename = filename

    def save(self, data: list, header: list):
        # Check the folder status:
        self._manage_directories()
        existing_record = Path(os.path.join(self._path, self._filename))
        if existing_record.exists():
            with open(os.path.join(self._path, self._filename), "a+") as log:
                record = csv.writer(log, delimiter=';')
                record.writerow(data)
        else:
            # If the file doesn't yet exist, create it with the header:
            with open(os.path.join(self._path, self._filename), "w+") as log:
                record = csv.writer(log, delimiter=';')
                record.writerow(header)
                record.writerow(data)

    def _manage_directories(self):
        """
        Creates the log folder the first time that the program is executed.
        """
        if not os.path.exists(self._path):
            os.makedirs(self._path)
            logging.info("Log directory not found. Creating it...")

    def obtain_latest_result(self, box: str, side: int):
        """
        Obtains the latest calibration value (arduino volume) in the log for a certain box and side.
        It will return None if the calibration file doesn't yet exist or if the record for that
        particular box is not present.
        """
        volumes = []
        last_volume = None

        if side == 0:
            side = "LR"
        elif side == 1:
            side = "L"
        else:
            side = "R"

        existing_record = Path(os.path.join(self._path, self._filename))
        if existing_record.exists():
            with open(os.path.join(self._path, self._filename), "r") as log:
               record = csv.DictReader(log, delimiter=';')
               for row in record:
                   if row['Box'] == box and row['Side'] == side:
                       volumes.append(row['Volume'])
            try:
                last_volume = int(volumes[-1])
            except IndexError:  # In case that this particular box hasn't been calibrated yet
                last_volume = None

        return last_volume
