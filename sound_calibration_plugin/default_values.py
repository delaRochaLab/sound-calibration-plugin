# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sound_calibration_plugin.settings as settings
import numpy as np

""" Calss to define the default variables. """

class DefaultValues:

    def __init__(self, windows):
        self.windows = windows
        self.val = {}
        try:
            self.__loadFromFile()
        except FileNotFoundError:
            self.__loadAllDefaultVariables()
            pass
        except Exception as err:
            print(err)

    def getValues(self):
        """ Return the default variables """
        return self.val

    def save(self):
        """ Save all the variable in a file """
        self.__recordVariables()
        np.save(settings.SOUND_PLUGIN_PATH_DEFAULT, self.val)

    def __loadFromFile(self):
        self.val = np.load(settings.SOUND_PLUGIN_PATH_DEFAULT)
        self.val = self.val.item()

    def __loadAllDefaultVariables(self):
        """ Set the default variables """
        # General
        self.val['_dbNeeds'] = '65'
        self.val['_tolerance'] = '0.5'
        self.val['_loop'] = '10'
        self.val['_side'] = 0

        # Pure tone
        self.val['_amp'] = '0.01'
        self.val['_freq'] = '1000'

        # Broadband
        self.val['_ampBroad'] = '0.2'
        self.val['_minBand'] = '2000'
        self.val['_maxBand'] = '20000'

        # Trend
        self.val['_minAmp'] = '0.01'
        self.val['_maxAmp'] = '1'
        self.val['_nAmp'] = '20'
        self.val['_fixFreq'] = '1000'

        self.val['_minFreq'] = '500'
        self.val['_maxFreq'] = '20000'
        self.val['_nFreq'] = '20'
        self.val['_fixAmp'] = '0.4'

        # Microphone
        self.val['_sens'] = '15'
        self.val['_gainM'] = '40'

        # SoundCard
        self.val['_fsOut'] = '192000'
        self.val['_devOut'] = 0

        # DAQ
        self.val['_fsIn'] = '500000'
        self.val['_chDqa'] = '0'
        self.val['_secDaq'] = '1'


    def __recordVariables(self):
        """ Record all the setting parameters """
        # General
        self.val['_dbNeeds'] = self.windows._dbNeeds.value
        self.val['_tolerance'] = self.windows._tolerance.value
        self.val['_loop'] = self.windows._loop.value
        self.val['_side'] = self.windows._side.value

        # Pure tone
        self.val['_amp'] = self.windows._amp.value
        self.val['_freq'] = self.windows._freq.value

        # Broadband
        self.val['_ampBroad'] = self.windows._ampBroad.value
        self.val['_minBand'] = self.windows._minBand.value
        self.val['_maxBand'] = self.windows._maxBand.value

        # Trend
        self.val['_minAmp'] = self.windows._minAmp.value
        self.val['_maxAmp'] = self.windows._maxAmp.value
        self.val['_nAmp'] = self.windows._nAmp.value
        self.val['_fixFreq'] = self.windows._fixFreq.value

        self.val['_minFreq'] = self.windows._minFreq.value
        self.val['_maxFreq'] = self.windows._maxFreq.value
        self.val['_nFreq'] = self.windows._nFreq.value
        self.val['_fixAmp'] = self.windows._fixAmp.value

        # Microphone
        self.val['_sens'] = self.windows._sens.value
        self.val['_gainM'] = self.windows._gainM.value

        # SoundCard
        self.val['_fsOut'] = self.windows._fsOut.value
        self.val['_devOut'] = self.windows._devOut.value

        # DAQ
        self.val['_fsIn'] = self.windows._fsIn.value
        self.val['_chDqa'] = self.windows._chDqa.value
        self.val['_secDaq'] = self.windows._secDaq.value



